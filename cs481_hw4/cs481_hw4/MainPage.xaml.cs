﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace cs481_hw4
{
    public partial class MainPage : ContentPage
    {

        List<Contact> contactlist = new List<Contact>();

        public class Contact
        {
            private string name;
            private string number;
            private short age;

            public override string ToString()  {
                return "Name : " + name + " Phone Number : " + number;
                }

            public Contact(string name, string number, short age)
            {
                this.name = name;
                this.age = age;
                this.number = number;
            }

            public string Name
            {
                get { return name; }
                set { name = value; }
            }
            public string Number
            {
                get { return number; }
                set { number = value; }
            }

            public short Age
            {
                get { return age; }
                set { age = value; }
            }
        }

        public MainPage()
        {
            InitializeComponent();
            ContactListView.RefreshCommand = new Command(() => {
                
                 ContactListView.IsRefreshing = false;

             });
            ContactListView.ItemTapped += Contact_Clicked;
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            // Creating fake contacts list
            contactlist.Add(new Contact("Josh", "(555)-555-5555", 21));
            contactlist.Add(new Contact("Joey", "(555)-555-5555", 21));
            contactlist.Add(new Contact("John", "(555)-555-5555", 21));
            contactlist.Add(new Contact("Rachel", "(555)-555-5555", 21));
            contactlist.Add(new Contact("Dexter", "(555)-555-5555", 21));
            contactlist.Add(new Contact("Jon", "(555)-555-5555", 21));
            contactlist.Add(new Contact("Mike", "(555)-555-5555", 21));
            contactlist.Add(new Contact("Daryl", "(555)-555-5555", 21));
            contactlist.Add(new Contact("Glenn", "(555)-555-5555", 21));
            contactlist.Add(new Contact("Rick", "(555)-555-5555", 21));
            contactlist.Add(new Contact("Morty", "(555)-555-5555", 21));
            contactlist.Add(new Contact("Roberto", "(555)-555-5555", 21));
            contactlist.Add(new Contact("Mamadou", "(555)-555-5555", 21));
            contactlist.Add(new Contact("Claude", "(555)-555-5555", 21));
            contactlist.Add(new Contact("Joey", "(555)-555-5555", 21));
            contactlist.Add(new Contact("Jack", "(555)-555-5555", 21));
            contactlist.Add(new Contact("Chirac", "(555)-555-5555", 21));
            contactlist.Add(new Contact("Nicolas", "(555)-555-5555", 21));
            contactlist.Add(new Contact("Alfred", "(555)-555-5555", 21));
            contactlist.Add(new Contact("Jessica", "(555)-555-5555", 21));
            ContactListView.ItemsSource = contactlist;
            
        }

        private void Delete_Clicked(object sender, EventArgs e)
        {
            var mi = ((MenuItem)sender);
            DisplayAlert("Test Delete", "Nothing Implemented Yet", "BACK");
        }
        private void Alert_Clicked(object sender, EventArgs e)
        {
            DisplayAlert("Context Menu Test", "Test ok", "BACK");
        }
        private void Contact_Clicked(object sender, ItemTappedEventArgs e)
        {
            if (e.Item == null) return;
            ContactDetail Contact_Page = new ContactDetail
            {
                BindingContext = e.Item as Contact
            };
            Navigation.PushAsync(Contact_Page);
  
        }
    }
}
